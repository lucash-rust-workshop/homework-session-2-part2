use std::env::args;
use std::fmt;
use std::fs::read_to_string;

enum Valuechange {
    Increment,
    Decrement,
}

impl fmt::Display for Valuechange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Valuechange::Increment => write!(f, "Increment"),
            Valuechange::Decrement => write!(f, "Decrement"),
        }
    }
}

enum RawInstruction {
    Pointer(Valuechange),
    Byte(Valuechange),
    Output,
    Input,
    WhileOpen,
    WhileClose,
}

impl RawInstruction {
    fn from_char(c: char) -> Option<Self> {
        match c {
            '>' => Some(RawInstruction::Pointer(Valuechange::Increment)),
            '<' => Some(RawInstruction::Pointer(Valuechange::Decrement)),
            '+' => Some(RawInstruction::Byte(Valuechange::Increment)),
            '-' => Some(RawInstruction::Byte(Valuechange::Decrement)),
            '.' => Some(RawInstruction::Output),
            ',' => Some(RawInstruction::Input),
            '[' => Some(RawInstruction::WhileOpen),
            ']' => Some(RawInstruction::WhileClose),
            _ => None,
        }
    }
}

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RawInstruction::Pointer(change) => write!(f, "Pointer {}", change),
            RawInstruction::Byte(change) => write!(f, "Byte {}", change),
            RawInstruction::Output => write!(f, "Output"),
            RawInstruction::Input => write!(f, "Input"),
            RawInstruction::WhileOpen => write!(f, "Start While"),
            RawInstruction::WhileClose => write!(f, "End While"),
        }
    }
}

struct Instruction {
    source: String,
    line: usize,
    column: usize,
    raw: RawInstruction,
}

impl Instruction {
    fn new(source: &str, line: usize, column: usize, raw: RawInstruction) -> Self {
        Instruction {
            source: String::from(source),
            line,
            column,
            raw,
        }
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[{}:{}:{}] {}",
            self.source, self.line, self.column, self.raw
        )
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input_file_name = args().nth(1).ok_or("No file name provided")?;
    let instructions = instructions_from_file(&input_file_name)?;

    instructions.iter().for_each(|i| println!("{}", i));

    Ok(())
}

// Take a file name and return a Result<Vec<Instruction>, Box<dyn std::error::Error>>
fn instructions_from_file(file_name: &str) -> Result<Vec<Instruction>, Box<dyn std::error::Error>> {
    let content = read_to_string(&file_name)?;

    Ok(content
        .lines()
        .filter(|line| !line.contains("---8<---")) // Remove the lines containing the input boundaries
        .enumerate()
        .flat_map(|(line_count, line)| {
            line.chars()
                .filter_map(RawInstruction::from_char) // Convert each char in RawInstruction Enum and remove the None
                .enumerate()
                .map(move |(char_count, instruction)| {
                    // Take ownership of the closure's context
                    Instruction::new(&file_name, line_count, char_count, instruction)
                })
        })
        .collect::<Vec<_>>())
}

// Alternate solution for the function which take 1 filename and returns a
// Result<Vec<StructFromAbove>, Box<dyn std::error:Error>>
// This solution uses nested for loops which seems less efficient as it pushes several times to a
// vector
// However I did not managed to run some benchmark to compare the two functions so I can't  back
// this intuition with any data
fn instructions_from_file_nested(
    file_name: &str,
) -> Result<Vec<Instruction>, Box<dyn std::error::Error>> {
    let content = read_to_string(&file_name)?;
    let mut instructions = Vec::new();

    for (line_count, line) in content.lines().enumerate() {
        if !line.contains("---8<---") { // Ignores the input boundaries
            for (char_count, c) in line.chars().enumerate() {
                let raw_instruction = RawInstruction::from_char(c); // Convert the character to a RawInstruction Enum
                if let Some(instruction) = raw_instruction { // If the raw_instruction is not None
                    instructions.push(Instruction::new(
                        &file_name,
                        line_count,
                        char_count,
                        instruction,
                    ));
                }
            }
        }
    }
    Ok(instructions)
}
